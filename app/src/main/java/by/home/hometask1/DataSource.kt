package by.home.hometask1

class DataSource {
    private val listSize: Int = 1000
    fun getItemList(): Array<Item> {
        val list: Array<Item> = Array(listSize) {
            i -> Item(i,
                "Title $i",
                "Description $i",
                R.drawable.baseline_emoji_emotions_24)
        }

        return list
    }
}