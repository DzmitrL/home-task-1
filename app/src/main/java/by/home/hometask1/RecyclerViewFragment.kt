package by.home.hometask1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView


class RecyclerViewFragment : Fragment() {

    private lateinit var model: SharedViewModel
    private lateinit var imageView: ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recycler_view_frag, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view)
        model = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)
        recyclerView.adapter = ItemsAdapter(DataSource().getItemList(),
            model,
            parentFragmentManager)


    }

    private fun adapterOnClick(item: Item) {
        parentFragmentManager.commit {
            setReorderingAllowed(true)
//            addSharedElement(imageView, "smile")
            replace(R.id.fragment_container_view, ItemDetailFragment())
        }
    }

}