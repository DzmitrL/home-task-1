package by.home.hometask1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.recyclerview.widget.RecyclerView

class ItemsAdapter(
    private val itemList: Array<Item>,
    private val model: SharedViewModel,
    private val parentFragmentManager: FragmentManager
) :
    RecyclerView.Adapter<ItemsAdapter.ItemViewHolder>() {


    class ItemViewHolder(itemView: View,
                         private val model: SharedViewModel,
                         parentFragmentManager: FragmentManager) : RecyclerView.ViewHolder(itemView) {

        private val itemTitleView: TextView = itemView.findViewById(R.id.item_title)
        private val itemDescriptionView: TextView = itemView.findViewById(R.id.item_description)
        private val itemImageView: ImageView = itemView.findViewById(R.id.item_image)
        private var currentItem: Item? = null

        init {
            itemView.setOnClickListener {
                currentItem?.let {
                    parentFragmentManager.commit {
                        setReorderingAllowed(true)
                        addSharedElement(itemImageView, "fragment_transition"
                        )
                        replace(R.id.fragment_container_view, ItemDetailFragment())
                    }
                }
                model.sendMessage(currentItem?.id.toString())
            }
        }

        fun bind(item: Item) {
            currentItem = item

            itemTitleView.text = item.title
            itemDescriptionView.text = item.description
            itemImageView.setImageResource(R.drawable.baseline_emoji_emotions_24)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row, parent, false)

        return ItemViewHolder(view, model, parentFragmentManager)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(itemList[position])
    }
}