package by.home.hometask1

import androidx.annotation.DrawableRes

class Item(
    val id: Int,
    val title: String,
    val description: String,
    @DrawableRes
    val image: Int?
)